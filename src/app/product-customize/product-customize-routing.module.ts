import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductCustomizePage } from './product-customize.page';

const routes: Routes = [
  {
    path: '',
    component: ProductCustomizePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductCustomizePageRoutingModule {}
