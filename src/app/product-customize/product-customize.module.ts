import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductCustomizePageRoutingModule } from './product-customize-routing.module';

import { ProductCustomizePage } from './product-customize.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductCustomizePageRoutingModule
  ],
  declarations: [ProductCustomizePage]
})
export class ProductCustomizePageModule {}
