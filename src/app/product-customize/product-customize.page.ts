import { Storage } from '@ionic/storage';
import { AppComponent } from './../app.component';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Component, NgZone, OnInit } from '@angular/core';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-product-customize',
  templateUrl: './product-customize.page.html',
  styleUrls: ['./product-customize.page.scss'],
})
export class ProductCustomizePage implements OnInit {
  product_id;
  product;
  quantite = 1;
  disabled_options = [];
  disabled_supplements = [];
  product_config = {
    id: null,
    quantite: null,
    options_groupe: [],
    supplements: [],
    ingredients: [],
  };
  allow_submit = true;
  panier;
  index;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private api: Services,
    private appComponent: AppComponent,
    private storage: Storage
  ) {}

  async get_product(id) {
    await this.api
      .product_details(id, this.appComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.product = res;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async ngOnInit() {
    this.route.params.subscribe((params) => {
      this.index = params['id'];
    });

    await this.storage.get('panier[]').then(data=>{
      this.panier = data;
      this.product_config=data[this.index];
    });

    this.product_id = this.product_config.id ;
    await this.get_product(this.product_id);
    this.initialize_disables();
    
    console.log('product_config: ', this.product_config);
    console.log('disabled_options: ', this.disabled_options);
    console.log('disabled_supplements: ', this.disabled_supplements);
  }

  initialize_disables(){
    this.allow_submit = true;
    let i = 0;
    this.product.options_groupes.forEach((groupe) => {
      this.disabled_options.push({ groupe_id: groupe.id, options: [] });
      groupe.options.forEach((option) => {
        this.disabled_options[i].options.push({id: option.id, disabled: false});
      });
      i++;
    });

    i=0;
    this.product.supplements.forEach((supplement) => {
      this.disabled_supplements.push({ id: supplement.id, disabled: false });
      i++;
    });
  }

  goBack() {
    this.navCtrl.back();
  }

  async on_submit() {
    this.panier[this.index] = this.product_config;
    this.storage.set('panier[]',this.panier);
    console.log('product_config: ', this.product_config);
  }

  define_submit(){
    let temp_allow_submit = true;
    let i = 0;
    this.product_config.options_groupe.forEach((options_groupe) => {
      
      if (options_groupe.options.length >= this.product.options_groupes[i].min) {
        temp_allow_submit = temp_allow_submit && true;
      }
      else {
        temp_allow_submit = false;
      }
      i++;
    });
    this.allow_submit = temp_allow_submit;
  }

  update_on_check(id, array){
    if (array.includes(id)) {
      for (var i = 0; i < array.length; i++) {
        if (array[i] == id) {
          array.splice(i, 1);
          return;
        }
      }
    }
    else {
      array.push(id);
      array.sort();
    }
  }

  option_isDisabled(id, index){
    return (this.product_config.options_groupe[index].options.length == this.product.options_groupes[index].max) && 
    !this.product_config.options_groupe[index].options.includes(id);
  }

  supplement_isDisabled(id){
    return (this.product_config.supplements.length == this.product.max_supplement) && !this.product_config.supplements.includes(id);
  }
}