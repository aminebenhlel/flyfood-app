import { MenuController } from '@ionic/angular';
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  panier;
  constructor(
    private menu: MenuController,
    private storage: Storage,
    private router: Router
  ) {}

  ionViewWillEnter() {
    this.menu.enable(true);
    this.onViewChaneg();
    this.getSToredProducts();
  }
  onViewChaneg() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        filter((event: NavigationEnd) =>
          event.urlAfterRedirects.includes('/tabs/')
        )
      )
      .subscribe((p) => {
        this.getSToredProducts();
      });
  }
  async getSToredProducts() {
    await this.storage.get('panier[]').then((data) => {
      this.panier = data;
    });
  }
}
