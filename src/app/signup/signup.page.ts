import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  showpass = 'password';
  signupForm: FormGroup;
  submitted = false;
  f: any;
  message = '';
  userExists;
  isNumber: boolean;

  constructor(
    private router: Router,
    private api: Services,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.validateform();
  }

  async ionViewWillEnter() {
    this.menu.enable(false);
    this.signupForm.reset();
    this.submitted = false;
  }

  showpassword() {
    this.showpass = this.showpass === 'password' ? 'text' : 'password';
  }

  validateform() {
    this.signupForm = new FormGroup({
      fullName: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(8),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
      pwd: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
    this.f = this.signupForm.controls;
  }

  async onSubmit() {
    this.submitted = true;
    this.message = '';
    if (isNaN(this.f.phone.value)) {
      this.isNumber = false;
      return;
    }
    if (this.signupForm.invalid && this.isNumber) {
      return;
    }
    const emailExist = await this.isEmailExist();
    if (emailExist) {
      this.message = 'Email unvailable !';
      return;
    }
    this.signUp();
  }
  async isEmailExist() {
    const req = {
      phone: this.f.phone.value,
      email: this.f.email.value,
    };
    return await this.api
      .emailExist(req)
      .toPromise()
      .then(
        (res: any) => {
          if (res.api_message === 'success') {
            this.message = 'Email unvailable !';
            return true;
          }
        },
        (err: any) => {
          this.message = 'Connection error !';
          return false;
        }
      );
  }

  async signUp() {
    const req = {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      full_name: this.f.fullName.value,
      phone: this.f.phone.value,
      email: this.f.email.value,
      password: this.f.pwd.value,
    };
    await this.api.signup(req).subscribe(
      (res: any) => {
        console.log('insrit');
        this.message = 'Success !';
        this.router.navigate(['/login']);
      },
      (err: any) => {
        this.message = 'Connection error !';
      }
    );
  }
}
