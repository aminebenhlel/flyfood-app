import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  status = 'Restaurant';
  restaurants;
  products;

  constructor(private api: Services, private appComponent: AppComponent) {}

  getRetaurants() {
    this.api.restaurants_listing(this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res);
        this.restaurants = res.data.filter((el) => el.is_favourite === 1);
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  getProducts() {
    this.api.products_listing(null, this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res.data);
        this.products = res.data.filter((el) => el.is_favourite === 1);
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }
  ngOnInit() {}

  ionViewWillEnter() {
    this.getRetaurants();
    this.getProducts();
  }
}
