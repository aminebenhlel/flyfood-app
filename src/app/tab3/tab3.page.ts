/* eslint-disable @typescript-eslint/prefer-for-of */
import { Router } from '@angular/router';
import { AppComponent } from './../app.component';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  panier = [];
  delivery;
  total;
  allowSubmit = false;
  adresse;
  constructor(
    private storage: Storage,
    private api: Services,
    private appComponent: AppComponent,
    private router: Router
  ) {}

  async ionViewWillEnter() {
    await this.getProductsConfig();
    this.set_total();
  }

  async getProductsConfig() {
    await this.storage.get('panier[]').then((data) => {
      console.log('panier stored data : ', data);
      if (data) {
        this.panier = data;
      }
    });
    if (this.panier) {
      for (let i = 0; i < this.panier.length; i++) {
        await this.getProductsDetails(i);
        await this.getOptionsAndSupplementsPrix(i);
      }
    }
    console.log('panier new data : ', this.panier);
  }

  async getProductsDetails(i) {
    await this.api
      .product_details(this.panier[i].id, null)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.panier[i].name = res.name;
          this.panier[i].prix = res.prix;
          this.panier[i].photo = res.photo;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async getOptionsAndSupplementsPrix(id) {
    for (let i = 0; i < this.panier[id].supplements.length; i++) {
      await this.api
        .get_supplement_prix(this.panier[id].supplements[i])
        .toPromise()
        .then(
          (res: any) => {
            this.panier[id].prix =
              parseFloat(this.panier[id].prix) + parseFloat(res.prix);
          },
          (err: any) => {
            console.log('api err', err);
          }
        );
    }

    for (let i = 0; i < this.panier[id].options_groupe.length; i++) {
      for (
        let j = 0;
        j < this.panier[id].options_groupe[i].options.length;
        j++
      ) {
        await this.api
          .get_option_prix(this.panier[id].options_groupe[i].options[j])
          .toPromise()
          .then(
            (res: any) => {
              this.panier[id].prix =
                parseFloat(this.panier[id].prix) + parseFloat(res.prix);
            },
            (err: any) => {
              console.log('api err', err);
            }
          );
      }
    }
  }

  delete_item(index) {
    this.panier.splice(index, 1);
    this.set_total();
    this.storage.set('panier[]', this.panier);
  }

  quantite(index, n) {
    if (n > 0) {
      this.panier[index].quantite = n;
      this.set_total();
      this.storage.set('panier[]', this.panier);
    }
  }

  async ngOnInit() {}

  set_total() {
    let total = 0;
    let delivery = 0;
    if (this.panier) {
      this.panier.forEach((config) => {
        total += config.prix * config.quantite;
      });
    }
    if (total * 0.1 >= 2) {
      delivery = total * 0.1;
    } else {
      delivery = 2;
    }
    this.delivery = delivery.toFixed(3);
    this.total = (total + delivery).toFixed(3);
  }

  async send_commande() {
    await this.storage.get('adresse').then((res: any) => {
      console.log('adress = ', res);
      this.adresse = res;
    });
    await this.api
      .send_commande(
        this.appComponent.user.id,
        this.total,
        this.adresse,
        this.panier
      )
      .toPromise()
      .then(
        (res: any) => {},
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  clear_panier() {
    this.storage.remove('panier[]');
    this.panier = [];
    this.total = (2).toFixed(3);
    this.delivery = (2).toFixed(3);
  }

  async on_submit() {
    if (this.appComponent.user.id === 0) {
      this.router.navigate(['login']);
    } else {
      await this.send_commande();
      this.allowSubmit = false;
      this.clear_panier();
      this.router.navigate(['/tabs/tab5']);
    }
  }
}
