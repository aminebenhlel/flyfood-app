import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StroageService {

  constructor(private storage: Storage) {
    
  }

  async get_data(){
    let res;
    await this.storage.get('user_info').then(data=>res=data);
    return res;
  }
}
