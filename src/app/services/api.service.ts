/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// Typescript custom enum for search types (optional)

@Injectable({
  providedIn: 'root',
})
export class Services {
  appUrl = 'http://144.91.100.67/flyfood/public/index.php/api/';
  constructor(private http: HttpClient) {}
  getHeader() {
    const timeNow = new Date().getTime().toString();
    const header: any = new HttpHeaders({
      'X-Authorization-Time': timeNow,
      'Content-Type': 'application/json',
    });
    return { headers: header };
  }

  login(postData) {
    const api = 'login';
    return this.http.post(this.appUrl + api, postData, this.getHeader());
  }

  signup(postData) {
    const api = 'signup';
    return this.http.post(this.appUrl + api, postData, this.getHeader());
  }

  emailExist(postData) {
    const api = 'user_isexist';
    return this.http.post(this.appUrl + api, postData, this.getHeader());
  }

  isUserUpdate(postData) {
    const api = 'is_user_update';
    return this.http.post(this.appUrl + api, postData, this.getHeader());
  }

  restaurants_listing(id_client) {
    const api = 'restaurants_listing';
    return this.http.post(this.appUrl + api, { id_client }, this.getHeader());
  }

  products_listing(id, id_client) {
    const api = 'products_listing';
    return this.http.post(
      this.appUrl + api,
      { id_restaurant: id, id_client },
      this.getHeader()
    );
  }

  categories_listing() {
    const api = 'categories_listing';
    return this.http.post(this.appUrl + api, this.getHeader());
  }

  restaurant_details(id, id_client) {
    const api = 'restaurant_details';
    return this.http.post(
      this.appUrl + api,
      { id, id_client },
      this.getHeader()
    );
  }

  product_details(id, id_client) {
    const api = 'product_details';
    return this.http.post(
      this.appUrl + api,
      { id, id_client },
      this.getHeader()
    );
  }

  set_restaurant_favourite(id_client, id_restaurant) {
    const api = 'set_restaurant_favourite';
    return this.http.post(
      this.appUrl + api,
      { id_client, id_restaurant },
      this.getHeader()
    );
  }

  set_product_favourite(id_client, id_produit) {
    const api = 'set_product_favourite';
    return this.http.post(
      this.appUrl + api,
      { id_client, id_produit },
      this.getHeader()
    );
  }

  get_option_prix(id) {
    const api = 'get_option_prix';
    return this.http.post(this.appUrl + api, { id }, this.getHeader());
  }

  get_supplement_prix(id) {
    const api = 'get_supplement_prix';
    return this.http.post(this.appUrl + api, { id }, this.getHeader());
  }

  send_commande(id_client, total, adresse, panier) {
    const api = 'send_commande';
    return this.http.post(
      this.appUrl + api,
      { id_client, total, adresse, panier },
      this.getHeader()
    );
  }

  get_commandes(id_client) {
    const api = 'get_commandes';
    return this.http.post(
      this.appUrl + api,
      { id_client },
      this.getHeader()
    );
  }

  get_client_adresses(id_client) {
    const api = 'get_client_adresses';
    return this.http.post(
      this.appUrl + api,
      { id_client },
      this.getHeader()
    );
  }

  set_client_adresse(id_client, adresse, id) {
    const api = 'set_client_adresse';
    return this.http.post(
      this.appUrl + api,
      { id_client, adresse, id },
      this.getHeader()
    );
  }

  delete_client_adresse(id) {
    const api = 'delete_client_adresse';
    return this.http.post(this.appUrl + api, { id }, this.getHeader());
  }

  update_user(full_name, phone, email, id) {
    const api = 'update_user';
    return this.http.post(
      this.appUrl + api,
      { full_name, phone, email, id },
      this.getHeader()
    );
  }
}
