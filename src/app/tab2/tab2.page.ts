import { AppComponent } from './../app.component';
import { NavController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  adresses;
  ch;
  newAddress;
  addNewAddrss = false;
  constructor(
    private navCtrl: NavController,
    private api: Services,
    private appComponent: AppComponent,
    private toastController: ToastController
  ) {}
  async ngOnInit() {
    await this.getClientAdresses();
  }
  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
  async getClientAdresses() {
    await this.api
      .get_client_adresses(this.appComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.adresses = res.data;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async updateClientAdresse(id, adresse) {
    console.log(adresse);
    await this.api
      .set_client_adresse(this.appComponent.user.id, adresse, id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api update client adresse res : ', res);
          this.presentToast('Address Updated successfully');
          this.getClientAdresses();
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async addClientAdresse(adresse) {
    if (!this.newAddress) {
      this.addNewAddrss = false;
      return;
    }
    await this.api
      .set_client_adresse(this.appComponent.user.id, this.newAddress, null)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.presentToast('Address added successfully');
          this.addNewAddrss = false;
          this.getClientAdresses();
          this.newAddress = '';
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  goBack() {
    this.navCtrl.back();
  }

  async delete(id) {
    await this.api
      .delete_client_adresse(id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
    await this.getClientAdresses();
  }
}
