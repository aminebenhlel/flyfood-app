import { Storage } from '@ionic/storage';
import { AppComponent } from './../app.component';
import { Services } from './../services/api.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  restaurants;
  products;
  categorie;
  selectedCategorie = -1;
  adresses;
  selectedAdresse;

  constructor(
    private router: Router,
    private api: Services,
    private appComponent: AppComponent,
    private storage: Storage
  ) {}
  async ionViewWillEnter() {
    this.getClientAdresses();
    this.getRetaurants();
    this.getProducts();
    this.categoriesListing();
  }
  getRetaurants() {
    this.api.restaurants_listing(this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res);
        this.restaurants = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  getProducts() {
    this.api.products_listing(null, this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res.data);
        this.products = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  categoriesListing() {
    this.api.categories_listing().subscribe(
      (res: any) => {
        console.log('api res : ', res.data);
        this.categorie = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  categorieClicked(id: any) {
    this.selectedCategorie = id;
  }

  isRestaurantIncludesCategorie(resto) {
    return (
      this.selectedCategorie === -1 ||
      this.selectedCategorie in resto?.categorie
    );
  }

  onClick(id) {
    this.router.navigate(['/restaurants', id]);
  }

  async getClientAdresses() {
    await this.api
      .get_client_adresses(this.appComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.adresses = res.data;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
    await this.storage.get('adresse').then((res: any) => {
      this.selectedAdresse = res;
    });
  }

  setAdresse() {
    console.log(this.selectedAdresse);
    this.storage.set('adresse', this.selectedAdresse);
  }
  isRestaurantListEmpty() {
    if (!this.restaurants) {
      return false;
    }
    let empty = true;
    this.restaurants.forEach((element) => {
      if (this.isRestaurantIncludesCategorie(element)) {
        empty = false;
      }
    });
    return empty;
  }
  isProductListEmpty() {
    if (!this.products) {
      return false;
    }
    let empty = true;
    this.products.forEach((element) => {
      if (
        this.selectedCategorie === -1 ||
        this.selectedCategorie === element.id_produit_categorie
      ) {
        empty = false;
      }
    });
    return empty;
  }
}
