import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public appPages = [
    { title: 'My orders', url: 'my-orders', icon: 'orders' },
    { title: 'My Profile', url: 'my-profile', icon: 'profile' },
    { title: 'Delivery Adresses', url: 'delivery-adresses', icon: 'adress' },
    { title: 'Contact Us', url: 'contact-us', icon: 'contact' },
    { title: 'Settings', url: 'settings', icon: 'settings' },
    { title: 'Help', url: 'help', icon: 'help' },
  ];
  user: any;

  constructor(
    private storage: Storage,
    private router: Router,
    private menu: MenuController
  ) {}
  async ngOnInit() {
    this.storage.get('user_info').then((data) => {
      this.user = data;
      console.log('user stored data : ', this.user);
    });
  }

  async logout() {
    this.storage.clear();
    //this.menu.close();
    this.router.navigate(['/login']);
  }
}
