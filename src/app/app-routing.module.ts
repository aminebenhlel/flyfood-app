import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'my-orders',
    loadChildren: () => import('./menu/my-orders/my-orders.module').then( m => m.MyOrdersPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./menu/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'delivery-adresses',
    loadChildren: () => import('./menu/delivery-adresses/delivery-adresses.module').then( m => m.DeliveryAdressesPageModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./menu/contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./menu/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./menu/help/help.module').then( m => m.HelpPageModule)
  }, 
  {
    path: 'restaurants/:id',
    loadChildren: () => import('./restaurants/restaurants.module').then( m => m.RestaurantsPageModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then( m => m.ProductsPageModule)
  },
  {
    path: 'product-detail/:id',
    loadChildren: () => import('./product-detail/product-detail.module').then( m => m.ProductDetailPageModule)
  },
  {
    path: 'restaurant-detail/:id',
    loadChildren: () => import('./restaurant-detail/restaurant-detail.module').then( m => m.RestaurantDetailPageModule)
  },
  {
    path: 'product-customize/:id',
    loadChildren: () => import('./product-customize/product-customize.module').then( m => m.ProductCustomizePageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
