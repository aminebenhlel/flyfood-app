import { AppComponent } from './../app.component';
import { MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Services } from '../services/api.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showpass = 'password';
  loginForm: FormGroup;
  submitted = false;
  connected = false;
  f: any;
  token: any;
  message: string;

  constructor(
    private router: Router,
    private api: Services,
    private storage: Storage,
    private menu: MenuController,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.validateform();
  }

  async ionViewWillEnter() {
    this.menu.enable(false);
    this.loginForm.reset();
    this.submitted = false;
  }
  validateform() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      pwd: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
    this.f = this.loginForm.controls;
  }

  showpassword() {
    this.showpass = this.showpass === 'password' ? 'text' : 'password';
  }

  async connectAsGuest() {
    await this.storage.set('user_info', { id: 0 });
    this.appComponent.user = { id: 0 };
    console.log('goo');
    this.router.navigate(['tabs']);
  }

  async onSubmit() {
    this.submitted = true;
    console.log(this.f.pwd.value);
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    } else {
      this.connected = true;
      await this.api
        .login({ email: this.f.email.value, password: this.f.pwd.value })
        .toPromise()
        .then(
          (res: any) => {
            if (!res.api_status) {
              this.message = 'Email or password incorrect !';
            } else if (res.status === 0) {
              this.message = 'Your account is suspended !';
            } else {
              this.message = 'Success !';
              this.appComponent.user = res;
              this.storage.set('user_info', res);
              this.router.navigate(['tabs']);
            }
            console.log('Api token res : ', res);
          },
          (err: any) => {
            this.message = 'Connection error !';
          }
        );
    }
  }
}
