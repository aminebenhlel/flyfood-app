import { AppComponent } from './../app.component';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.page.html',
  styleUrls: ['./restaurant-detail.page.scss'],
})
export class RestaurantDetailPage implements OnInit {
  user_id;
  restaurant_id;
  resto;
  products;
  choosed;
  id_resto;
  id_product;
  categorie;
  isChoosed = 0;
  isFavourite: boolean;
  categories;
  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private api: Services,
    private appComponent: AppComponent
  ) {}

  async get_restaurant(id) {
    await this.api
      .restaurant_details(id, this.appComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.resto = res;
          // this.categories = Object.values(res?.categorie);
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  get_products() {
    this.api
      .products_listing(this.restaurant_id, this.appComponent.user.id)
      .subscribe(
        (res: any) => {
          console.log('api product res : ', res['data']);
          this.products = res['data'];
          this.choosed = this.products;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async ngOnInit() {
    this.route.params.subscribe((params) => {
      this.restaurant_id = params['id'];
    });
    
    await this.get_restaurant(this.restaurant_id);
    this.isFavourite = this.resto.is_favourite;
    this.categorie = Object.values(this.resto['categories']);

    await this.get_products();
  }

  swipe_categorie(id) {
    this.isChoosed = id;
    if (id == -1) {
      this.choosed = this.products;
    } else {
      this.choosed = this.products.filter(
        (element) => element.id_produit_categorie == id
      );
    }
  }

  async swipe_favourite() {
    await this.api
      .set_restaurant_favourite(this.appComponent.user.id, this.restaurant_id)
      .toPromise()
      .then();
    this.isFavourite = !this.isFavourite;
  }

  goBack() {
    this.navCtrl.back();
  }
}
