import { AppComponent } from './../app.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.page.html',
  styleUrls: ['./restaurants.page.scss'],
})
export class RestaurantsPage implements OnInit {
  isFavourite: boolean;
  status = 'Food Items';
  getValue;
  restaurants;
  savedRestaurants;
  products;
  categorie;
  selected_categorie = null;
  savedProducts;
  sort = 1;
  searchKey;
  temp;
  constructor(private route: ActivatedRoute, private api: Services, private appComponent: AppComponent) { }
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.status = params['id'];
    });
    this.get_retaurants();
    this.get_products();
    this.categories_listing();
  }
  get_retaurants() {
    this.api.restaurants_listing(this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res);
        this.restaurants = res.data;
        this.savedRestaurants = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  get_products() {
    this.api.products_listing(null, this.appComponent.user.id).subscribe(
      (res: any) => {
        console.log('api res : ', res['data']);
        this.savedProducts = res.data;
        this.products = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  categorie_clicked(cat: any) {
    this.selected_categorie = cat;
    this.restaurants = this.savedRestaurants;
    this.products = this.savedProducts;
    if (!cat) {
      return;
    }
    this.products = this.savedProducts.filter((el) => {
      return el.produit_categorie_name == cat.name;
    });
    this.restaurants = this.savedRestaurants.filter((el) => {
      return JSON.stringify(el.categorie).includes(cat.name);
    });

  }

  categories_listing() {
    this.api.categories_listing().subscribe(
      (res: any) => {
        console.log('api res : ', res.data);
        this.categorie = res.data;
      },
      (err: any) => {
        console.log('api err', err);
      }
    );
  }

  is_restaurant_includes_categorie(restaurants) {
    return this.selected_categorie == -1 || (this.selected_categorie in restaurants?.categorie)
  }


  sortProiducts() {
    this.sort *= -1;
    this.products.sort((firstEl, secondEl) => {
      if (firstEl.prix > secondEl.prix) {
        return this.sort * -1;
      }
      else {
        return this.sort;
      }
    });
  }
  search() {
    this.restaurants = this.savedRestaurants;
    this.products = this.savedProducts;
    this.selected_categorie = null;
    if (!this.searchKey) {
      return;
    }
    this.products = this.savedProducts.filter((el) => {
      return el.name.toLowerCase().includes(this.searchKey.toLowerCase());
    });
    this.restaurants = this.savedRestaurants.filter((el) => {
      return el.name.toLowerCase().includes(this.searchKey.toLowerCase());
    });
  }
}
