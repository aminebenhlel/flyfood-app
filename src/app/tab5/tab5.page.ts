import { AppComponent } from './../app.component';
import { NavController, Platform } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Services } from '../services/api.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  status;
  stat;
  commandes = [];
  commandes_previous = [];
  interval;
  constructor(
    private navCtrl: NavController,
    private api: Services,
    private AppComponent: AppComponent,
    private notificatin: LocalNotifications,
    private plt: Platform
  ) {}

  push_notif(message) {
    let isAndroid = this.plt.is('android');
    this.notificatin.schedule({
      id: 1,
      text: message,
      sound: isAndroid ? 'file://sound.mp3' : 'file://beep.caf',
      data: { secret: 'key' },
    });
  }

  async get_commandes() {
    await this.api
      .get_commandes(this.AppComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          this.commandes = res.data;
          console.log(this.commandes);
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async get_commandes_real_time() {
    this.interval = setInterval(() => {

      for(let i=0; i<this.commandes.length; i++){
        if(this.commandes[i].status != this.commandes_previous[i].status){
          console.log('the order N°: '+this.commandes[i].id+' is '+this.commandes[i].status);
          this.push_notif('the order N°: '+this.commandes[i].id+' is '+this.commandes[i].status);
        }
      }
  
      this.commandes_previous = this.commandes;
      this.get_commandes();

    }, 3000);
  }

  async ionViewWillEnter() {
    await this.get_commandes();
    this.commandes_previous = this.commandes;
    this.get_commandes_real_time(); 
  }

  ionViewWillLeave() {
    clearInterval(this.interval);
    console.log('interval cleared');
  }
  async ngOnInit() {
    this.status = 'upcoming';
  }

  goBack() {
    this.navCtrl.back();
  }
}
