import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-restuarnt-small',
  templateUrl: './restuarnt-small.component.html',
  styleUrls: ['./restuarnt-small.component.scss'],
})
export class RestuarntSmallComponent implements OnInit {
  @Input() resto;
  isFavourite = false;
  constructor() {}

  ngOnInit() {
    console.log(this.resto);
  }

  swipeFavourite() {
    this.isFavourite = !this.isFavourite;
  }
}
