import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss'],
})
export class RestaurantComponent implements OnInit {
  @Input() resto;
  isFavourite: boolean;
  categories;
  constructor() {}
  ngOnInit() {
    this.categories = Object.values(this.resto.categorie);
  }
}
