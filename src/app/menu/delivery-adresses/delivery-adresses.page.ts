import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery-adresses',
  templateUrl: './delivery-adresses.page.html',
  styleUrls: ['./delivery-adresses.page.scss'],
})
export class DeliveryAdressesPage implements OnInit {
  constructor(private route:Router) { }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.route.navigate(['/tabs/tab2']);
  }

}
