import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveryAdressesPageRoutingModule } from './delivery-adresses-routing.module';

import { DeliveryAdressesPage } from './delivery-adresses.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryAdressesPageRoutingModule
  ],
  declarations: [DeliveryAdressesPage]
})
export class DeliveryAdressesPageModule {}
