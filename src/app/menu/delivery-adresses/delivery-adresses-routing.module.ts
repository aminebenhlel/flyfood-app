import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryAdressesPage } from './delivery-adresses.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveryAdressesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveryAdressesPageRoutingModule {}
