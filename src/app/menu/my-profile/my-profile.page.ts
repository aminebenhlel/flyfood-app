import { AppComponent } from '../../app.component';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Services } from 'src/app/services/api.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {
  showpass = 'password';
  signupForm: FormGroup;
  submitted = false;
  f: any;
  message = '';
  userExists;
  isNumber: boolean;

  constructor(
    private route: Router,
    private api: Services,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.validateform();
  }

  async ionViewWillEnter() {
    this.signupForm.reset();
    this.submitted = false;
  }

  showpassword() {
    this.showpass = this.showpass === 'password' ? 'text' : 'password';
  }

  validateform() {
    this.signupForm = new FormGroup({
      fullName: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(8),
      ]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });
    this.f = this.signupForm.controls;
  }

  async onSubmit() {
    let userExists = false;
    this.submitted = true;
    this.message = '';
    console.log(this.f.phone.value);
    if (isNaN(this.f.phone.value)) {
      this.isNumber = false;
    } else {
      this.isNumber = true;
    }
    console.log('is number : ', this.isNumber);

    if (this.signupForm.invalid || !this.isNumber) {
      return;
    } else {
      console.log('valid');

      await this.api
        .emailExist({ phone: this.f.phone.value, email: this.f.email.value })
        .toPromise()
        .then(
          (res: any) => {
            if (res.api_message === 'success') {
              userExists = true;
              this.message = 'Email or phone unvailable !';
            }
          },
          (err: any) => {
            this.message = 'Connection error !';
          }
        );

      console.log(this.message);

      if (userExists === false) {
        await this.api
          .update_user(
            this.f.fullName.value,
            this.f.phone.value,
            this.f.email.value,
            this.appComponent.user.id
          )
          .toPromise()
          .then(
            (res: any) => {
              this.message = 'Success !';
            },
            (err: any) => {
              this.message = 'Connection error !';
            }
          );
      }
    }
  }
  goBack() {
    this.route.navigate(['tabs']);
  }
}
