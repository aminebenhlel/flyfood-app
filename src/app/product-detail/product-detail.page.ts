import { Storage } from '@ionic/storage';
import { AppComponent } from './../app.component';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Services } from '../services/api.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  product_id;
  product;
  isFavourite : boolean;
  quantite = 1;
  disabled_options = [];
  disabled_supplements = [];
  product_config = {
    id: null,
    quantite: null,
    options_groupe: [],
    supplements: [],
    ingredients: [],
  };
  allow_submit = true;
  panier;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private api: Services,
    private appComponent: AppComponent,
    private storage: Storage
  ) {}

  async get_product(id) {
    await this.api
      .product_details(id, this.appComponent.user.id)
      .toPromise()
      .then(
        (res: any) => {
          console.log('api resto res : ', res);
          this.product = res;
        },
        (err: any) => {
          console.log('api err', err);
        }
      );
  }

  async ngOnInit() {
    this.route.params.subscribe((params) => {
      this.product_id = params['id'];
    });
    await this.get_product(this.product_id);
    this.isFavourite = this.product.is_favourite;
    this.initialize_disables();
    
    console.log('product_config: ', this.product_config);
    console.log('disabled_options: ', this.disabled_options);
    console.log('disabled_supplements: ', this.disabled_supplements);
  }

  initialize_disables(){
    let i = 0;
    this.product.options_groupes.forEach((groupe) => {
      if (groupe.min > 0) {
        this.allow_submit = false;
      }
      this.product_config.options_groupe.push({ id: groupe.id, options: [] });
      this.disabled_options.push({ groupe_id: groupe.id, options: [] });
      groupe.options.forEach((option) => {
        this.disabled_options[i].options.push({id: option.id, disabled: false});
      });
      i++;
    });

    this.product.supplements.forEach((supplement) => {
      this.disabled_supplements.push({ id: supplement.id, disabled: false });
    });

    this.product.ingredients.forEach((ingredient) => {
      this.product_config.ingredients.push(ingredient.id);
    });
  }

  goBack() {
    this.navCtrl.back();
  }

  are_equal(array1, array2): boolean{
    return JSON.stringify(array1) == JSON.stringify(array2);
  }

  exists(object, array): boolean{
    let ret =false;
    array.forEach(element => {
        if(object.id == element.id){
          if(this.are_equal(object.ingredients, element.ingredients)){
            if(this.are_equal(object.supplements, element.supplements)){
              if(this.are_equal(object.options_groupe, element.options_groupe)){
                element.quantite += object.quantite;
                this.storage.set('panier[]', array);
                ret = true;
              }
            }
          }
        }

    });
    return ret;
  }

  add_product_config(panier, product){
    if(panier == null){
      let array = [this.product_config];
      console.log("panier vide : ", array);
      
      this.storage.set('panier[]', array);
    }
    else if(!this.exists(product, panier)){
      panier.push(this.product_config);
      console.log("panier non vide mais config introuvable : ", panier);
      
      this.storage.set('panier[]', panier);
    }
  }

  async swipe_favourite() {
    await this.api
      .set_product_favourite(this.appComponent.user.id, this.product_id)
      .toPromise()
      .then();
    this.isFavourite = !this.isFavourite;
  }

  async on_submit() {
    this.product_config.id = this.product.id;
    this.product_config.quantite = this.quantite;
    await this.storage.get('panier[]').then(data=>{
      this.panier=data;
    });
    this.add_product_config(this.panier, this.product_config);

    console.log('product_config: ', this.product_config);
  }

  define_submit(){
    let temp_allow_submit = true;
    let i = 0;
    this.product_config.options_groupe.forEach((options_groupe) => {
      if (options_groupe.options.length >= this.product.options_groupes[i].min) {
        temp_allow_submit = temp_allow_submit && true;
      }
      else {
        temp_allow_submit = false;
      }
      i++;
    });
    this.allow_submit = temp_allow_submit;
  }

  update_on_check(id, array){
    if (array.includes(id)) {
      for (var i = 0; i < array.length; i++) {
        if (array[i] == id) {
          array.splice(i, 1);
          return;
        }
      }
    } else {
      array.push(id);
      array.sort();
    }
  }

  option_isDisabled(id, index){
    return (this.product_config.options_groupe[index].options.length == this.product.options_groupes[index].max) && 
    !this.product_config.options_groupe[index].options.includes(id);
  }

  supplement_isDisabled(id){
    return (this.product_config.supplements.length == this.product.max_supplement) && !this.product_config.supplements.includes(id);
  }
}